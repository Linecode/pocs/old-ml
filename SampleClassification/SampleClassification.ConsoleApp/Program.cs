//*****************************************************************************************
//*                                                                                       *
//* This is an auto-generated file by Microsoft ML.NET CLI (Command-Line Interface) tool. *
//*                                                                                       *
//*****************************************************************************************

using System;
using SampleClassification.Model;

namespace SampleClassification.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create single instance of sample data from first line of dataset for model input
            ModelInput sampleData = new ModelInput()
            {
                ID = 1F,
                Title = @"Apple iPhone 11 Pro Max, US Version, 64GB, Gold - Unlocked (Renewed)",
            };

            // Make a single prediction on the sample data and print results
            var predictionResult = ConsumeModel.Predict(sampleData);

            Console.WriteLine("Using model to make single prediction -- Comparing actual Category with predicted Category from sample data...\n\n");
            Console.WriteLine($"ID: {sampleData.ID}");
            Console.WriteLine($"Title: {sampleData.Title}");
            Console.WriteLine($"\n\nPredicted Category value {predictionResult.Prediction} \nPredicted Category scores: [{String.Join(",", predictionResult.Score)}]\n\n");
            Console.WriteLine("=============== End of process, hit any key to finish ===============");
            Console.ReadKey();
        }
    }
}
