﻿using System;
using Microsoft.ML;
using SampleClassification.Model;

namespace consumeModelApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            MLContext mlContext = new MLContext();
            ITransformer mlModel = mlContext.Model.Load("MLModel.zip", out var modelInputSchema);
            var predEngine = mlContext.Model.CreatePredictionEngine<ModelInput, ModelOutput>(mlModel);

            var input = new ModelInput();

            input.Title = "apple";

            ModelOutput result = predEngine.Predict(input);
            
            Console.WriteLine(result.Prediction);
        }
    }
}
